/*! [PROJECT_NAME] | January 2015 | Suitmedia */

;(function ( window, document, undefined ) {

    var path = {
        css: myPrefix + 'assets/css/',
        js : myPrefix + 'assets/js/vendor/'
    };

    var assets = {
        _jquery_cdn     : '//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js',
        _jquery_local   : path.js + 'jquery.min.js',
        _fastclick      : path.js + 'fastclick.min.js',
        _slick          : path.js + 'slick.min.js'
    };

    var Site = {

        init: function () {
            Site.fastClick();
            Site.enableActiveStateMobile();
            Site.WPViewportFix();
            Site.slider();
        },

        fastClick: function () {
            Modernizr.load({
                load    : assets._fastclick,
                complete: function () {
                    FastClick.attach(document.body);
                }
            });
        },

        enableActiveStateMobile: function () {
            if ( document.addEventListener ) {
                document.addEventListener('touchstart', function () {}, true);
            }
        },

        WPViewportFix: function () {
            if ( navigator.userAgent.match(/IEMobile\/10\.0/) ) {
                var style   = document.createElement("style"),
                    fix     = document.createTextNode("@-ms-viewport{width:auto!important}");

                style.appendChild(fix);
                document.getElementsByTagName('head')[0].appendChild(style);
            }
        },

        slider: function () {
            var $banner = $('.banner');

            if ( !$banner.length ) return ;

            $banner.slick({
                arrows: false,
                dots: true,
                customPaging: function (slider, i) {
                    return '<button class="nav-banner" type="button" data-role="none">' + (i + 1) + '</button>';
                }
            });
        }

    };

    var siteInit = function () {
        Site.init();
    };

    var checkJquery = function () {
        Modernizr.load([
            {
                test    : window.jQuery,
                nope    : assets._jquery_local,
                complete: siteInit
            }
        ]);
    };

    /*Modernizr.load({
        load    : assets._jquery_cdn,
        complete: checkJquery
    });*/

    Modernizr.load([
        {
            load    : assets._jquery_local
        },
        {
            load    : assets._slick,
            complete: checkJquery
        }
    ]);

    window.Site = Site;

})( window, document );
